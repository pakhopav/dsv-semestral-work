package cz.ctu.fee.dsv.semework;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class BusinessController extends Thread implements Serializable {
    private Node myNode;
    private ArrayList<String> sharedMapKeys = new ArrayList(Arrays.asList("a", "b", "c", "d", "e"));
    private int iterations = 100;

    public void setIterations(int iterations) {
        this.iterations = iterations;
    }

    public BusinessController(Node myNode) {
        this.myNode = myNode;
    }

    @Override
    public void run() {
        Random r = new Random();
        for (int i = 0; i < iterations; ++i) {
            if (Thread.interrupted()) break;
            int rInt = r.nextInt(10) * 300;
            int rInt2 = r.nextInt(10);
            int rInt3 = r.nextInt(5);
            if (rInt < 500) {
                myNode.setVariable(sharedMapKeys.get(rInt3), rInt2);
            } else {
                myNode.getVariable(sharedMapKeys.get(rInt3));
            }
            try {
                Thread.sleep(rInt);


            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


}
