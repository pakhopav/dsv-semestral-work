package cz.ctu.fee.dsv.semework;

import cz.ctu.fee.dsv.semework.base.Address;
import cz.ctu.fee.dsv.semework.base.DSNeighbours;
import cz.ctu.fee.dsv.semework.base.NodeCommands;

import java.rmi.RemoteException;

public class MessageReceiver implements NodeCommands {
    private Node myNode;
    private Boolean criticalSectionIsLocked = false;

    public MessageReceiver(Node node) {
        myNode = node;
    }


    @Override
    public DSNeighbours join(Address addr) throws RemoteException {
        Node.LOGGER.info("JOIN was called ...");
        if (addr.compareTo(myNode.getAddress()) == 0) {
            myNode.becomeLeader();
            Node.LOGGER.info("I am the first and leader");
            return myNode.getNeighbours();
        } else {
            Node.LOGGER.info("Someone is joining ...");
            DSNeighbours myNeighbours = myNode.getNeighbours();
            Address myInitialNext = new Address(myNeighbours.next);     // because of 2 nodes config
            Address myInitialPrev = new Address(myNeighbours.prev);     // because of 2 nodes config
            DSNeighbours tmpNeighbours = new DSNeighbours(myNeighbours.next,
                    myNeighbours.nnext,
                    myNode.getAddress(),
                    myNeighbours.leader);
            // to my next send msg ChPrev to addr
            myNode.getCommHub().getNext().changePrev(addr);
            // to my prev send msg ChNNext addr
            myNode.getCommHub().getRMIProxy(myInitialPrev).changeNNext(addr);
            tmpNeighbours.nnext = myNeighbours.nnext;

            myNeighbours.next = addr;
            myNeighbours.nnext = myInitialNext;
            return tmpNeighbours;
        }
    }

    @Override
    public Address changeNext(Address addr) throws RemoteException {
        Node.LOGGER.info("ChangeNext was called ...");
        myNode.getNeighbours().next = addr;
        return myNode.getNeighbours().prev;
    }

    @Override
    public void changeNNext(Address addr) throws RemoteException {
        Node.LOGGER.info("ChangeNNext was called ...");
        myNode.getNeighbours().nnext = addr;
    }


    @Override
    public Address changePrev(Address addr) throws RemoteException {
        Node.LOGGER.info("ChangePrev was called ...");
        myNode.getNeighbours().prev = addr;
        return myNode.getNeighbours().next;
    }


    @Override
    public void nodeMissing(Address addr) throws RemoteException {
        Node.LOGGER.info("NodeMissing was called with " + addr);
        if (addr.compareTo(myNode.getNeighbours().next) == 0) {
            // its for me
            DSNeighbours myNeighbours = myNode.getNeighbours();
            // to my nnext send msg ChPrev with myaddr -> my nnext = next
            myNeighbours.next = myNeighbours.nnext;
            myNeighbours.nnext = myNode.getCommHub().getNNext().changePrev(myNode.getAddress());
            // to my prev send msg ChNNext to my.next
            myNode.getCommHub().getPrev().changeNNext(myNeighbours.next);
            Node.LOGGER.info("NodeMissing DONE");
        } else {
            // send to next node
            myNode.getCommHub().getNext().nodeMissing(addr);
        }
    }

    @Override
    public void leaderDisconnected() throws RemoteException {
        Node.LOGGER.info("LeaderDisconnected received");
        myNode.startLeaderElection();
    }

    @Override
    public void topologyCheck(long id) throws RemoteException {
        Node.LOGGER.info("Topology check process started");
        if (myNode.getNodeId() != id) {
            if (id == -1) {
                myNode.getCommHub().getNext().topologyCheck(myNode.getNodeId());
            } else {
                myNode.getCommHub().getNext().topologyCheck(id);
            }
        } else {
            Node.LOGGER.info("Topology is checked, OK.");
        }
    }


    @Override
    public void messageOne(long id) throws RemoteException {
        if (myNode.state == VotingState.ACTIVE) {
            Node.LOGGER.info("[Active] MESSAGE ONE RECEIVED");
            if (id == -1) {
                myNode.isElectionInitiator = true;
                myNode.getCommHub().getNext().messageOne(myNode.cip);
                return;
            }

            if (myNode.cip == id) {
                myNode.getCommHub().getNext().messageSmall(id, null);
                return;
            }

            myNode.acnp = id;
            if (myNode.isElectionInitiator) {
                Node.LOGGER.info("[Election initiator] MESSAGE ONE CYCLE ENDED");
                myNode.getCommHub().getNext().messageTwo(myNode.acnp);
            } else {
                myNode.getCommHub().getNext().messageOne(myNode.cip);
            }

        } else {
            Node.LOGGER.info("[Passive] MESSAGE ONE RECEIVED");
            myNode.getCommHub().getNext().messageOne(id);
        }
    }

    @Override
    public void messageTwo(long id) throws RemoteException {
        if (myNode.state == VotingState.ACTIVE) {
            Node.LOGGER.info("[Active] MESSAGE TWO RECEIVED");

            if (myNode.acnp < id && myNode.acnp < myNode.cip) {
                Node.LOGGER.info("[Active] CIP CHANGED FROM " + myNode.cip + " TO " + myNode.acnp);
                myNode.cip = myNode.acnp;
            } else {
                Node.LOGGER.info("[Active] BECOMING PASSIVE");
                myNode.state = VotingState.PASSIVE;

            }

            if (myNode.isElectionInitiator) {
                Node.LOGGER.info("[Election initiator] MESSAGE TWO CYCLE ENDED");
                Node.LOGGER.info("[Election initiator] NEW STAGE STARTED");
                if (myNode.state == VotingState.PASSIVE) {
                    myNode.isElectionInitiator = false;
                    myNode.getCommHub().getNext().messageOne(-1);
                } else myNode.getCommHub().getNext().messageOne(myNode.cip);
            } else {
                myNode.getCommHub().getNext().messageTwo(myNode.acnp);
            }


        } else {
            Node.LOGGER.info("[Passive] MESSAGE TWO RECEIVED");
            myNode.getCommHub().getNext().messageTwo(id);
        }
    }

    @Override
    public void messageSmall(long id, Address leaderAddress) throws RemoteException {
        if (leaderAddress != null) {
            Node.LOGGER.info("SMALL RECEIVED WITH ADDRESS, CANDIDATE ID: " + id);
            myNode.getNeighbours().leader = leaderAddress;
            myNode.cip = myNode.getNodeId();
            myNode.acnp = -1;
            myNode.state = VotingState.ACTIVE;
            myNode.isElectionInitiator = false;
            if (myNode.getNodeId() != id) {
                myNode.getCommHub().getNext().messageSmall(id, leaderAddress);
            } else {
                Node.LOGGER.info("ELECTION ENDED. NEW LEADER: " + leaderAddress);
            }
        } else {
            Node.LOGGER.info("SMALL RECEIVED WITH NULL, CANDIDATE ID: " + id);
            if (myNode.getNodeId() == id) {
                myNode.becomeLeader();
                myNode.getCommHub().getNext().messageSmall(id, myNode.getAddress());
            } else {
                myNode.getCommHub().getNext().messageSmall(id, null);
            }
        }
    }

    @Override
    public int getVariable(String key, String token) {
        if (myNode.isLeader()) {
            LeaderContext context = myNode.leaderContext;
            if (context.actualToken.equals(token)) {
                Node.LOGGER.info("[Leader] Process enters critical section");
                return context.sharedMap.get(key);
            } else {
                Node.LOGGER.warning("[Leader] Process tried to enter CS with WRONG TOKEN!");
            }
        } else {
            Node.LOGGER.warning("[Leader] getVariable() method called on NON Leader node");
        }
        return -1;
    }


    @Override
    public void setVariable(String key, int nValue, String token) {
        if (myNode.isLeader()) {
            LeaderContext context = myNode.leaderContext;
            if (context.actualToken.equals(token)) {
                Node.LOGGER.info("[Leader] Process enters critical section");
                if (!context.sharedMap.containsKey(key)) {
                    Node.LOGGER.warning("[Leader] No such key in map: " + key);
                    return;
                }
                context.sharedMap.put(key, nValue);
            }
        } else {
            Node.LOGGER.warning("[Leader] getVariable() method called on NON Leader node");
        }

    }

    @Override
    public void requireToken(Address sender) {
        if (!myNode.isLeader()) {
            Node.LOGGER.warning("Someone required a token from me, but im NOT the leader");
            return;
        }

        if (!criticalSectionIsLocked) {
            try {
                provideToken(sender);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        } else {
            Node.LOGGER.info("[Leader] Process added to the queue");
            myNode.leaderContext.queue.add(sender);
        }
    }

    @Override
    public void takeToken(String token) {
        Node.LOGGER.info("Receiving the token..");
        if (myNode.isLeader()) {
            LeaderContext context = myNode.leaderContext;
            if (context.actualToken.equals(token)) {
                criticalSectionIsLocked = false;
            }

            //Pridej token dalsimu procesu pukud ceka
            if (!context.queue.isEmpty()) {
                Address nextNodeAddress = context.queue.remove(0);
                try {
                    provideToken(nextNodeAddress);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        } else {
            myNode.myToken = token;
        }
    }

    private void provideToken(Address receiver) throws RemoteException {
        if (!myNode.isLeader()) return;
        String newToken = myNode.leaderContext.regenerateToken();
        if (receiver.compareTo(myNode.getAddress()) != 0) {
            Node.LOGGER.info("[Leader] Providing the process on addr:" + receiver + " with token..");
            myNode.getCommHub().getRMIProxy(receiver).takeToken(newToken);
        } else {
            myNode.myToken = newToken;
        }
        criticalSectionIsLocked = true;
    }

}
