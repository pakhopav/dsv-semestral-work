package cz.ctu.fee.dsv.semework.base;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface NodeCommands extends Remote {

    //Topology-configuration commands
    public DSNeighbours join(Address addr) throws RemoteException;

    public Address changeNext(Address addr) throws RemoteException;

    public void changeNNext(Address addr) throws RemoteException;

    public Address changePrev(Address addr) throws RemoteException;

    public void nodeMissing(Address addr) throws RemoteException;

    public void leaderDisconnected() throws RemoteException;

    public void topologyCheck(long id) throws RemoteException;

    //Leader election algorithm commands
    public void messageOne(long id) throws RemoteException;

    public void messageTwo(long id) throws RemoteException;

    public void messageSmall(long id, Address leaderAddress) throws RemoteException;

    //Shared variable commands
    public int getVariable(String key, String token) throws RemoteException;

    public void setVariable(String key, int nValue, String token) throws RemoteException;

    //Critical section commands
    public void requireToken(Address sender) throws RemoteException;
    public void takeToken(String token) throws RemoteException;

}