package cz.ctu.fee.dsv.semework;

public enum VotingState {
    ACTIVE, PASSIVE
}
