package cz.ctu.fee.dsv.semework;

import cz.ctu.fee.dsv.semework.base.Address;
import cz.ctu.fee.dsv.semework.base.DSNeighbours;
import cz.ctu.fee.dsv.semework.base.NodeCommands;

import java.io.IOException;
import java.io.Serializable;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

public class Node implements Runnable, Serializable {
    // Using logger is strongly recommended (log4j, ...)

    // Name of our RMI "service"
    public static final String COMM_INTERFACE_NAME = "DSVNode";
    public static final Logger LOGGER;

    static {
        LOGGER = Logger.getLogger("DSV Logger");
        LOGGER.setUseParentHandlers(false);
        ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new DsvConsoleLoggerFormatter());
        LOGGER.addHandler(handler);
        try {
            FileHandler fh = new FileHandler("LogFile.txt");
            LOGGER.addHandler(fh);
            fh.setFormatter(new DsvLoggerFormatter());
        } catch (SecurityException | IOException e) {
            e.printStackTrace();
        }
    }

    // This Node
    public static Node thisNode = null;
    public String myToken = null;
    LeaderContext leaderContext = null;
    boolean repairInProgress = false;
    boolean isInSlowmode = false;

    // Initial configuration from commandline
    private String nickname = "Unknown";
    private String myIP = "127.0.0.1";
    private int myPort = 2010;
    private String otherNodeIP = "127.0.0.1";
    private int otherNodePort = 2010;
    private long nodeId = 0;
    private Address myAddress;
    private DSNeighbours myNeighbours;
    private NodeCommands myMessageReceiver;
    private CommunicationHub myCommHub;
    private DsvConsoleHandler myConsoleHandler;
    private BusinessController myBusinessController = null;

    //voting properties
    public long cip = -1;
    public long acnp = -1;
    public VotingState state = VotingState.ACTIVE;
    public Boolean isElectionInitiator = false;


    public Node(String[] args) {


        // handle commandline arguments
        if (args.length == 3) {
            nickname = args[0];
            myIP = otherNodeIP = args[1];
            myPort = otherNodePort = Integer.parseInt(args[2]);
        } else if (args.length == 5) {
            nickname = args[0];
            myIP = args[1];
            myPort = Integer.parseInt(args[2]);
            otherNodeIP = args[3];
            otherNodePort = Integer.parseInt(args[4]);
        } else {
            LOGGER.severe("Wrong number of commandline parameters - using default values.");
        }
    }

    public static void main(String[] args) {


        thisNode = new Node(args);
        thisNode.run();
    }

    private long generateId(String address, int port) {
        // generates  <port><IPv4_dec1><IPv4_dec2><IPv4_dec3><IPv4_dec4>
        String[] array = myIP.split("\\.");
        long id = 0;
        long shift = 0, temp = 0;
        for (int i = 0; i < array.length; i++) {
            temp = Long.parseLong(array[i]);
            id = (long) (id * 1000);
            id += temp;
        }
        if (id == 0) {
            id = 666000666000L;
        }
        id = id + port * 1000000000000L;
        return id;
    }

    private NodeCommands startMessageReceiver() {
        System.setProperty("java.rmi.server.hostname", myAddress.hostname);

        NodeCommands msgReceiver = null;
        try {
            msgReceiver = new MessageReceiver(this);

            NodeCommands skeleton = (NodeCommands) UnicastRemoteObject.exportObject(msgReceiver, 40000 + myAddress.port);

            Registry registry = LocateRegistry.createRegistry(myAddress.port);
            registry.rebind(COMM_INTERFACE_NAME, skeleton);
        } catch (Exception e) {
            LOGGER.info("Message listener - something is wrong: " + e.getMessage());
        }
        LOGGER.info("Message listener is started ...");

        return msgReceiver;
    }

    @Override
    public String toString() {
        return "Node:\n  [id:'" + nodeId + "',\n" +
                "   nick:'" + nickname + "',\n" +
                "   ip:'" + myIP + "',\n" +
                "   port:'" + myPort + "']\n";
    }

    public void printStatus() {
        LOGGER.info("==========Status:==========\n" + this + "With neighbours:\n" + myNeighbours + "===========================");
    }

    @Override
    public void run() {
        nodeId = generateId(myIP, myPort);
        cip = nodeId;
        myAddress = new Address(myIP, myPort);
        myNeighbours = new DSNeighbours(myAddress);
        printStatus();
        ensureValidTopology();
        myMessageReceiver = startMessageReceiver();
        if (myMessageReceiver == null) {
            LOGGER.severe("Node STOPPED working");
            return;
        }
        myCommHub = new CommunicationHub(this);

        myConsoleHandler = new DsvConsoleHandler(this);

        // JOIN

        try {
            NodeCommands tmpNode = myCommHub.getRMIProxy(new Address(otherNodeIP, otherNodePort));
            myNeighbours = tmpNode.join(myAddress);
        } catch (RemoteException e) {
            LOGGER.severe("Node didn`t join the system");
            return;
        }
        LOGGER.info("Neighbours after JOIN:\n" + myNeighbours);

        new Thread(myConsoleHandler).run();
    }

    public synchronized void repairTopology(Address missingNodeAddress) {
        if (!repairInProgress) {
            repairInProgress = true;
            {
                try {
                    myMessageReceiver.nodeMissing(missingNodeAddress);
                } catch (RemoteException e) {
                    // this should not happen
                    e.printStackTrace();
                }
                LOGGER.info("Topology was repaired " + myNeighbours);
            }
            repairInProgress = false;

            // test leader

            try {
                Address leader = myNeighbours.leader;
                LocateRegistry.getRegistry(leader.hostname, leader.port).lookup(Node.COMM_INTERFACE_NAME);
                ;
            } catch (NotBoundException | RemoteException e) {
                startLeaderElection();
            }
        }
    }


    public Address getAddress() {
        return myAddress;
    }


    public DSNeighbours getNeighbours() {
        return myNeighbours;
    }


    public NodeCommands getMessageReceiver() {
        return myMessageReceiver;
    }


    public CommunicationHub getCommHub() {
        return myCommHub;
    }


    public long getNodeId() {
        return nodeId;
    }


    public boolean isLeader() {
        return leaderContext != null;
    }


    public void setVariable(String key, int value) {
        LOGGER.info("Starting SetVariable process..");
        getTokenFromLeader();

        try {
            LOGGER.info("Entered the Critical section");
            if (isInSlowmode) {
                for (int i = 0; i < 5; ++i) {
                    try {
                        Thread.sleep(1000);
                        LOGGER.info("Slowly doing my work..");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            myCommHub.getLeader().setVariable(key, value, myToken);
            LOGGER.info("New value was set: [" + key + "," + value + "]");
            LOGGER.info("Exiting the Critical section");
            returnTokenToLeader();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public int getVariable(String key) {
        LOGGER.info("Starting GetVariable process..");
        int val = -1;
        getTokenFromLeader();
        try {
            LOGGER.info("Entered the Critical section");
            if (isInSlowmode) {
                for (int i = 0; i < 5; ++i) {
                    try {
                        Thread.sleep(1000);
                        LOGGER.info("Slowly doing my work..");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            val = myCommHub.getLeader().getVariable(key, myToken);
            LOGGER.info("Got variable: " + key + " = " + val);
            LOGGER.info("Exiting the Critical section");
            returnTokenToLeader();

        } catch (RemoteException e) {
            e.printStackTrace();
        }

        return val;
    }

    public void getTokenFromLeader() {
        try {
            myCommHub.getLeader().requireToken(this.myAddress);
        } catch (RemoteException e) {
            try {
                myCommHub.getLeader().requireToken(this.myAddress);
            } catch (RemoteException remoteException) {
                remoteException.printStackTrace();
            }
        }
        while (myToken == null) {
            try {
                LOGGER.info("Waiting for token..");
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        LOGGER.info("Got the token!");
    }

    public void disconnect() {
        try {
            LOGGER.info("Disconnecting...");
            Address myInitialNext = new Address(myNeighbours.next);
            Address myInitialNNext = new Address(myNeighbours.nnext);
            Address myInitialPrev = new Address(myNeighbours.prev);
            if (myInitialNext.compareTo(myAddress) == 0 && myInitialNNext.compareTo(myInitialPrev) == 0) {
                //only 1 node in system
                myConsoleHandler.stop();
                return;
            } else if (myInitialNext.compareTo(myInitialPrev) == 0) {
                //2 nodes in system
                NodeCommands previousNode = myCommHub.getPrev();
                previousNode.changeNext(myInitialNext);
                previousNode.changeNNext(myInitialNext);
                previousNode.changePrev(myInitialNext);
            } else {
                // >2 nodes
                Address pprev = myCommHub.getPrev().changeNext(myInitialNext);
                myCommHub.getPrev().changeNNext(myInitialNNext);
                myCommHub.getRMIProxy(pprev).changeNNext(myInitialNext);
                myCommHub.getNext().changePrev(myInitialPrev);
            }
            myConsoleHandler.stop();
            if (isLeader() && myAddress.compareTo(myNeighbours.next) != 0) {
                myCommHub.getNext().leaderDisconnected();
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void startLeaderElection() {
        try {
            LOGGER.info("Starting Leader Election Peterson algorithm..");
            isElectionInitiator = true;
            myCommHub.getNext().messageOne(cip);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void becomeLeader() {
        leaderContext = new LeaderContext();
    }

    public void startWork() {
        myBusinessController = new BusinessController(this);
        LOGGER.info("Starting work simulation, 100 iterations");
        new Thread(myBusinessController).run();
    }

    public void startWork(int iterations) {
        myBusinessController = new BusinessController(this);
        myBusinessController.setIterations(iterations);
        LOGGER.info("Starting work simulation, " + iterations + " iterations");

        new Thread(myBusinessController).run();
    }

    public void stopWork() {
        if (myBusinessController == null) return;
        myBusinessController.interrupt();
        LOGGER.info("Work was stopped");
    }

    private void returnTokenToLeader() throws RemoteException {
        LOGGER.info("Returning the token to the leader..");
        myCommHub.getLeader().takeToken(myToken);
        myToken = null;
    }

    private void ensureValidTopology() {
        Address otherAddr = new Address(otherNodeIP, otherNodePort);
        if (myAddress.compareTo(otherAddr) == 0) return;
        try {
            NodeCommands otherNodeRemote = (NodeCommands) LocateRegistry.getRegistry(otherNodeIP, otherNodePort).lookup(Node.COMM_INTERFACE_NAME);
            otherNodeRemote.topologyCheck(-1);
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }
}


