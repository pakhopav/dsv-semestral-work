package cz.ctu.fee.dsv.semework;

import java.util.Date;
import java.util.logging.LogRecord;
import java.util.logging.SimpleFormatter;

public class DsvConsoleLoggerFormatter extends SimpleFormatter {
    private static final String format = "%2$-7s \n %3$s %n";

    @Override
    public synchronized String format(LogRecord lr) {
        return String.format(format,
                new Date(lr.getMillis()),
                lr.getLevel().getLocalizedName(),
                lr.getMessage()
        );
    }
}