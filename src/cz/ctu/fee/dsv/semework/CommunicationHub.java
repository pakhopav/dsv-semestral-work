package cz.ctu.fee.dsv.semework;

import cz.ctu.fee.dsv.semework.base.Address;
import cz.ctu.fee.dsv.semework.base.DSNeighbours;
import cz.ctu.fee.dsv.semework.base.NodeCommands;

import java.io.Serializable;
import java.rmi.ConnectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class CommunicationHub implements Serializable {
    private Node myNode;
    private Address myAddress = null;
    private NodeCommands myMessageReceiver = null;

    public CommunicationHub(Node node) {
        myNode = node;
        this.myAddress = node.getAddress();
        this.myMessageReceiver = node.getMessageReceiver();
    }

    private DSNeighbours getNeighbours() {
        return myNode.getNeighbours();
    }

    public NodeCommands getNext() throws RemoteException {
        return getRMIProxy(getNeighbours().next);
    }


    public NodeCommands getNNext() throws RemoteException {
        return getRMIProxy(getNeighbours().nnext);
    }


    public NodeCommands getPrev() throws RemoteException {
        return getRMIProxy(getNeighbours().prev);
    }


    public NodeCommands getLeader() throws RemoteException {
        return getRMIProxy(getNeighbours().leader);
    }


    public NodeCommands getRMIProxy(Address address) throws RemoteException {
        if (address.compareTo(myAddress) == 0) return myMessageReceiver;
        else {
            try {
                Registry registry = LocateRegistry.getRegistry(address.hostname, address.port);
                return (NodeCommands) registry.lookup(Node.COMM_INTERFACE_NAME);
            } catch (ConnectException | NotBoundException nbe) {
                Node.LOGGER.warning("Seems like the node " + address + " is missing. Starting repair topology");
                myNode.repairTopology(address);
                throw new RemoteException(nbe.getMessage());
            }
        }
    }

}