package cz.ctu.fee.dsv.semework;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

public class DsvConsoleHandler implements Runnable, Serializable {

    private boolean reading = true;
    private BufferedReader reader = null;
    private PrintStream out = System.out;
    private PrintStream err = System.err;
    private Node myNode;


    public DsvConsoleHandler(Node myNode) {
        this.myNode = myNode;
        reader = new BufferedReader(new InputStreamReader(System.in));
    }


    private void parse_commandline(String commandline) {
        if (commandline.equals("s")) {
            myNode.printStatus();
        } else if (commandline.equals("?")) {
            Node.LOGGER.info("? - this help\ns - print node status\nset <variable name> <new value> - set a new value to the shared variable\n" +
                    "get <variable name> - get shared variable\nls - [on leader] prints the actual shared var map\n" +
                    "chmod <mode name> - changes the working mode <mode name> = \"s\"(slow) OR \"n\"(normal)\n" +
                    "d - disconnect correctly from the system\nw <optional number of iterations>- to start the working simulation(100 iterations by default)\n"
            );
        } else if (commandline.startsWith("set")) {
            String[] strings = commandline.split(" ");
            if (strings.length == 3) {
                String keyString = strings[1];
                String valueString = strings[2];
                int value = Integer.parseInt(valueString);
                myNode.setVariable(keyString, value);
            }
        } else if (commandline.startsWith("get")) {
            String[] strings = commandline.split(" ");
            if (strings.length == 2) {
                String keyString = strings[1];
                int val = myNode.getVariable(keyString);
            }
        } else if (commandline.equals("ls")) {
            if (myNode.isLeader()) {
                List<String> mapItems = myNode.leaderContext.sharedMap.entrySet().stream().map(it -> "[" + it.getKey() + "," + it.getValue() + "]").collect(Collectors.toList());
                String out = "=============================\nNode " + myNode.getNodeId() + "\n";
                for (String s : mapItems) {
                    out += s + "\n";
                }
                out += "=============================\n";
                Node.LOGGER.info(out);
            } else {
                Node.LOGGER.warning("Im not leader and cant show you the Variable map");
            }

        } else if (commandline.startsWith("work")) {
            String[] strings = commandline.split(" ");
            if (strings.length == 2) {
                int iterations = Integer.parseInt(strings[1]);
                myNode.startWork(iterations);
            } else {
                myNode.startWork();
            }

        } else if (commandline.equals("d")) {
            myNode.disconnect();

        } else if (commandline.startsWith("chmod")) {
            String[] strings = commandline.split(" ");
            if (strings.length == 2) {
                String mode = strings[1];
                if (mode.equals("s")) {
                    Node.LOGGER.info("Switched to slow mode");
                    myNode.isInSlowmode = true;
                } else if (mode.equals("n")) {
                    Node.LOGGER.info("Switched to normal mode");
                    myNode.isInSlowmode = false;

                } else {
                    Node.LOGGER.warning("Unrecognized command.");
                }
            }
        } else {
            // do nothing
            Node.LOGGER.warning("Unrecognized command.");
        }
    }


    @Override
    public void run() {
        String commandline = "";
        while (reading) {
            commandline = "";
            System.out.print("\ncmd > ");
            try {
                commandline = reader.readLine();
                parse_commandline(commandline);
            } catch (IOException e) {
                err.println("ConsoleHandler - error in rading console input.");
                e.printStackTrace();
                reading = false;
            }
        }
        Node.LOGGER.info("Closing ConsoleHandler.");
    }

    public void stop() {
        reading = false;
    }
}