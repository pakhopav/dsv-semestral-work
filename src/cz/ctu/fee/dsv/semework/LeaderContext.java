package cz.ctu.fee.dsv.semework;

import cz.ctu.fee.dsv.semework.base.Address;

import java.io.Serializable;
import java.util.*;

public class LeaderContext implements Serializable {

    public String actualToken = "";
    public Map<String, Integer> sharedMap = new HashMap<>() {{
        put("a", 0);
        put("b", 0);
        put("c", 0);
        put("d", 0);
        put("e", 0);
    }};
    public List<Address> queue = new ArrayList<>();

    public LeaderContext() {
        regenerateToken();
    }

    public String regenerateToken() {
        int randomNumber = new Random().nextInt(1000000);
        String newToken = "" + randomNumber;
        actualToken = newToken;
        return newToken;
    }
}
